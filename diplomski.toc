\babel@toc {croatian}{}
\contentsline {chapter}{\numberline {1}Uvod}{1}{chapter.1}%
\contentsline {paragraph}{}{1}{section*.2}%
\contentsline {paragraph}{}{1}{section*.3}%
\contentsline {chapter}{\numberline {2}Strojno učenje}{2}{chapter.2}%
\contentsline {section}{\numberline {2.1}Dijelovi algoritma strojnog učenja}{2}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Funkcija pogreške}{2}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}Optimizacijski postupak}{2}{subsection.2.1.2}%
\contentsline {subsection}{\numberline {2.1.3}Model}{3}{subsection.2.1.3}%
\contentsline {paragraph}{}{3}{section*.4}%
\contentsline {paragraph}{}{3}{section*.5}%
\contentsline {paragraph}{}{4}{section*.7}%
\contentsline {paragraph}{}{4}{section*.8}%
\contentsline {paragraph}{}{4}{section*.9}%
\contentsline {section}{\numberline {2.2}Testiranje modela}{5}{section.2.2}%
\contentsline {paragraph}{}{5}{section*.10}%
\contentsline {paragraph}{}{5}{section*.11}%
\contentsline {paragraph}{}{5}{section*.13}%
\contentsline {section}{\numberline {2.3}Generativni modeli i diskriminativni modeli}{6}{section.2.3}%
\contentsline {paragraph}{}{6}{section*.14}%
\contentsline {paragraph}{}{6}{section*.15}%
\contentsline {paragraph}{}{6}{section*.16}%
\contentsline {paragraph}{}{6}{section*.17}%
\contentsline {paragraph}{}{6}{section*.18}%
\contentsline {section}{\numberline {2.4}Duboko učenje}{7}{section.2.4}%
\contentsline {paragraph}{}{7}{section*.19}%
\contentsline {chapter}{\numberline {3}Neuronske mreže}{8}{chapter.3}%
\contentsline {section}{\numberline {3.1}Neuronske mreže}{8}{section.3.1}%
\contentsline {paragraph}{}{8}{section*.20}%
\contentsline {section}{\numberline {3.2}Građa neuronske mreže}{8}{section.3.2}%
\contentsline {paragraph}{}{8}{section*.21}%
\contentsline {paragraph}{}{8}{figure.caption.23}%
\contentsline {paragraph}{}{9}{section*.24}%
\contentsline {paragraph}{}{9}{section*.25}%
\contentsline {section}{\numberline {3.3}Aktivacijske funkcije}{9}{section.3.3}%
\contentsline {paragraph}{}{9}{section*.27}%
\contentsline {paragraph}{}{9}{section*.28}%
\contentsline {subsection}{\numberline {3.3.1}Primjeri aktivacijskih funkcija}{10}{subsection.3.3.1}%
\contentsline {subsubsection}{Linearna}{10}{section*.29}%
\contentsline {subsubsection}{ReLU}{10}{section*.30}%
\contentsline {subsubsection}{Sigmoidna funkcija}{10}{section*.31}%
\contentsline {subsubsection}{Tangens hiperbolni ($tanh$)}{10}{section*.32}%
\contentsline {subsubsection}{Softmax}{10}{section*.33}%
\contentsline {subsection}{\numberline {3.3.2}Primjena aktivacijskih funkcija}{11}{subsection.3.3.2}%
\contentsline {paragraph}{}{11}{section*.34}%
\contentsline {paragraph}{}{11}{section*.35}%
\contentsline {paragraph}{}{11}{section*.36}%
\contentsline {section}{\numberline {3.4}Slojevi neuronskih mreža}{12}{section.3.4}%
\contentsline {paragraph}{}{12}{section*.37}%
\contentsline {subsection}{\numberline {3.4.1}Potpuno povezani sloj}{12}{subsection.3.4.1}%
\contentsline {paragraph}{}{12}{section*.38}%
\contentsline {subsection}{\numberline {3.4.2}Konvolucijski sloj}{12}{subsection.3.4.2}%
\contentsline {paragraph}{}{12}{section*.39}%
\contentsline {paragraph}{}{12}{section*.40}%
\contentsline {paragraph}{}{12}{section*.41}%
\contentsline {paragraph}{}{13}{section*.42}%
\contentsline {subsection}{\numberline {3.4.3}Sloj sažimanja}{13}{subsection.3.4.3}%
\contentsline {paragraph}{}{13}{section*.44}%
\contentsline {paragraph}{}{13}{section*.45}%
\contentsline {section}{\numberline {3.5}Treniranje neuronske mreže}{13}{section.3.5}%
\contentsline {paragraph}{}{13}{section*.46}%
\contentsline {paragraph}{}{14}{section*.47}%
\contentsline {paragraph}{}{14}{section*.48}%
\contentsline {paragraph}{}{14}{section*.49}%
\contentsline {chapter}{\numberline {4}Generativne suparničke mreže}{15}{chapter.4}%
\contentsline {section}{\numberline {4.1}Uvod}{15}{section.4.1}%
\contentsline {paragraph}{}{15}{section*.50}%
\contentsline {paragraph}{}{15}{section*.51}%
\contentsline {paragraph}{}{15}{section*.52}%
\contentsline {paragraph}{}{15}{section*.53}%
\contentsline {section}{\numberline {4.2}Treniranje generativno suparničke mreže}{16}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Usporedbe distribucija podatka}{16}{subsection.4.2.1}%
\contentsline {paragraph}{}{16}{section*.54}%
\contentsline {paragraph}{}{16}{section*.55}%
\contentsline {paragraph}{}{16}{section*.56}%
\contentsline {subsection}{\numberline {4.2.2}Funkcije pogreške}{17}{subsection.4.2.2}%
\contentsline {paragraph}{}{17}{section*.57}%
\contentsline {subsubsection}{Standardna pogreška}{17}{section*.58}%
\contentsline {paragraph}{}{17}{section*.59}%
\contentsline {paragraph}{}{17}{section*.60}%
\contentsline {paragraph}{}{17}{section*.61}%
\contentsline {subsubsection}{Poteškoće tijekom treniranja}{17}{section*.62}%
\contentsline {paragraph}{}{17}{section*.63}%
\contentsline {paragraph}{}{18}{section*.64}%
\contentsline {paragraph}{}{18}{section*.65}%
\contentsline {subsubsection}{Wasserstainov gubitak}{18}{section*.66}%
\contentsline {paragraph}{}{18}{section*.67}%
\contentsline {section}{\numberline {4.3}Implementacija generativno suparničke mreže}{19}{section.4.3}%
\contentsline {subsection}{\numberline {4.3.1}Korištene tehnologije}{19}{subsection.4.3.1}%
\contentsline {paragraph}{}{19}{section*.68}%
\contentsline {paragraph}{}{19}{section*.69}%
\contentsline {paragraph}{}{19}{section*.70}%
\contentsline {subsection}{\numberline {4.3.2}Skupovi za učenje}{19}{subsection.4.3.2}%
\contentsline {subsubsection}{UTKFace}{19}{section*.71}%
\contentsline {paragraph}{}{19}{section*.72}%
\contentsline {paragraph}{}{19}{section*.73}%
\contentsline {subsubsection}{CelebA}{20}{section*.75}%
\contentsline {paragraph}{}{20}{section*.76}%
\contentsline {paragraph}{}{20}{section*.78}%
\contentsline {subsubsection}{Implementacija rukovanja skupom podataka}{21}{section*.79}%
\contentsline {paragraph}{}{21}{section*.80}%
\contentsline {paragraph}{}{21}{section*.81}%
\contentsline {paragraph}{}{21}{section*.82}%
\contentsline {subsection}{\numberline {4.3.3}Tehnički podaci}{22}{subsection.4.3.3}%
\contentsline {paragraph}{}{22}{section*.83}%
\contentsline {subsubsection}{Arhitektura mreže}{24}{section*.86}%
\contentsline {paragraph}{}{24}{section*.87}%
\contentsline {paragraph}{}{24}{section*.89}%
\contentsline {subsection}{\numberline {4.3.4}Rezultati generiranja}{25}{subsection.4.3.4}%
\contentsline {subsubsection}{UTKFace}{25}{section*.91}%
\contentsline {paragraph}{}{25}{section*.92}%
\contentsline {paragraph}{}{26}{section*.94}%
\contentsline {paragraph}{}{26}{section*.97}%
\contentsline {subsubsection}{CelebA}{28}{section*.100}%
\contentsline {paragraph}{}{28}{section*.101}%
\contentsline {section}{\numberline {4.4}Ostale implementacije}{30}{section.4.4}%
\contentsline {subsubsection}{Implementacija Erika Lindera-Noréna}{30}{section*.106}%
\contentsline {paragraph}{}{30}{section*.107}%
\contentsline {subsubsection}{Implementacija Minjong Kima}{31}{section*.109}%
\contentsline {paragraph}{}{31}{section*.110}%
\contentsline {chapter}{\numberline {5}Zaključak}{32}{chapter.5}%
\contentsline {paragraph}{}{32}{section*.113}%
\contentsline {paragraph}{}{32}{section*.115}%
\contentsline {paragraph}{}{32}{section*.116}%
\contentsline {chapter}{Literatura}{33}{chapter*.117}%
\contentsline {chapter}{Popis slika}{35}{chapter*.118}%
\contentsline {chapter}{Popis tablica}{36}{chapter*.119}%
\contentsline {paragraph}{}{37}{section*.120}%
\contentsline {paragraph}{}{37}{section*.121}%
\contentsline {paragraph}{}{38}{section*.122}%
\contentsline {paragraph}{}{38}{section*.123}%
